# This file contains code that will generate and compile a casadi nmpc model
from re import I
import sys
sys.path.append("../")
import numpy as np
import casadi as ca
import casadi.tools as ctools
import matplotlib.pyplot as plt

from pathlib import Path

stage_error_weight=0.15
stage_control_weight=0.000005
terminal_error_weight=6

#placeholder
c_rocket = 4
c_rudder = 5
m = 100
l=0.2
l_rocket = 5
r_rocket = 0.2
tm=0.25 * m * r_rocket**2 + 1/12 * m * l_rocket**2
gravity = 9.81
prediction_horizon = 50
dt = 0.1

tor_upper = 30
tor_lower = 25
tor_x = 25

# State Variables
x_x = ca.SX.sym('x_x')
x_vx = ca.SX.sym('x_vx')
x_y = ca.SX.sym('x_y')
x_vy = ca.SX.sym('x_vy')
x_phi = ca.SX.sym('x_phi')
x_omega = ca.SX.sym('x_omega')
x_theta = ca.SX.sym('x_theta')

# Control Variables
# These are angular velocities, not accelerations!!! Values are correct, just the naming is not
u_F = ca.SX.sym('u_F')
u_theta = ca.SX.sym('u_theta')

# odes
x_dot = x_vx
vx_dot = (1/m)*(ca.cos(x_phi)*u_F-c_rocket*(x_vx**2+x_vy**2)*ca.cos(x_phi))
y_dot = x_vy
vy_dot = (1/m)*((ca.sin(x_phi)*u_F-c_rocket*(x_vx**2+x_vy**2)*ca.sin(x_phi))) - gravity
phi_dot = x_omega
omega_dot = -c_rudder*(x_vx**2+x_vy**2)*ca.sin(x_theta)*l/tm
theta_dot = u_theta

# Concatenated vectors
statevector = ca.vertcat(x_x, x_vx, x_y, x_vy, x_phi, x_omega, x_theta)
controlvector = ca.vertcat(u_F, u_theta)
statedynamics = ca.vertcat(x_dot, vx_dot, y_dot, vy_dot , phi_dot, omega_dot,
                           theta_dot)

# Creating ode dict
ode = dict(x=statevector,p=controlvector, ode=statedynamics)

# Defining NLP variables as a structure
W = ctools.struct_symMX([
    (
        ctools.entry('x', shape=(7, 1), repeat=prediction_horizon+1),
        ctools.entry('u', shape=(2, 1), repeat=prediction_horizon),
    )
])

# RK4 integration function for the robot dynamics
M = 1
DT = dt
f = ca.Function('f', [statevector, controlvector], [
                statedynamics])
X0 = ca.MX.sym('X0', 7)
U = ca.MX.sym('U', 2)
X = X0
for j in range(M):
    k1 = f(X, U)
    k2 = f(X+DT/2 * k1, U)
    k3 = f(X+DT/2 * k2, U)
    k4 = f(X+DT * k3, U)
    X = X+DT/6*(k1+2*k2+2*k3+k4)
robot_dynamics_function = ca.Function(
    'F', [X0, U], [X], ['x0', 'p'], ['xf'])

pv = ca.MX.sym('pv', (2, 1))

# Empty constraint and objective function
g = []
J = 0


# Looping over the timesteps in the prediction horizon
for k in range(prediction_horizon):
    # Get current variables
    xk = W['x', k]
    uk = W['u', k]

    # Add continuity constraint
    xk_next = robot_dynamics_function(x0=xk, p=uk)['xf']
    g.append(xk_next - W['x', k+1])
    # g.append(ca.norm_2(xk[[0, 2]] - [4, 5]) - 1.5)
    g.append(xk[2] - (xk[0] - tor_x)**2 - tor_upper)
    g.append(xk[2] + (xk[0] - tor_x)**2 - tor_lower)

    # xk = xk_next
    #J += stage_error_weight*((xk[0] - pv[0])**2 + (xk[2] - pv[1])**2)
    J += stage_control_weight*uk[0]**2  # control penalty
    J += stage_control_weight*uk[1]**2  # control penalty
    J += stage_error_weight*((xk[0] - pv[0])**2 + (xk[2] - pv[1])**2)

# Add terminal cost
xk = xk_next
J += terminal_error_weight*((xk[0] - pv[0])**2 + (xk[2] - pv[1])**2)

# Create the final NLP with options to compile it
nlp = dict(x=ca.vertcat(W), p=pv, g=ca.vertcat(*g), f=J)


jit_options = {"verbose": True}
options_sol = dict(
    print_time=0, 
    ipopt=dict(
        print_level=0
        ), 
    verbose=False) 

# Create the solver for the nlp with the given options
trajectory_planner = ca.nlpsol('S', 'ipopt', nlp, options_sol)


##############################
##############################
##############################
##############################
##############################

# Initial state
rocket_state = np.array([0, 0, 0, 0, np.pi/2, 0, 0])

# Desired goal state
goal = np.array([tor_x*2, 1])

rocket_state_traj = [rocket_state]
control_traj = []
max_iter = 300  
iter = 0

# Bounds on the state and the controls
ubw = W(np.inf)
lbw = W(-np.inf)

ubw['x', :] = ca.DM([np.inf, np.inf, np.inf, np.inf, np.inf, np.inf, np.pi/4])
lbw['x', :] = ca.DM([-np.inf, -np.inf, 0, -np.inf, -np.inf, -np.inf, -np.pi/4])

ubw['u', :] = ca.DM([2000, 1])
lbw['u', :] = ca.DM([0, -1])      


# Create empty lists for the uper and lower bound on constraints and the parametervector
ubg = []
lbg = []

max_fail_counter = prediction_horizon-1
fail_counter = 0

for k in range(prediction_horizon):
    # Bounds for the dynamic constraints
    ubg += 7*[0]
    lbg += 7*[0]

    # ubg += [np.inf]
    # lbg += [0]

    ubg += [0]
    lbg += [-np.inf]

    ubg += [np.inf]
    lbg += [0]

# Setting up the first initial guess to contain all 0
wopt = W(0)

while np.linalg.norm(np.array([rocket_state[0] - goal[0], rocket_state[2] -
                               goal[1]])) > 1 and iter < max_iter:
    iter += 1
    # create casadi matrix of current state
    x0 = rocket_state

    # Initialize the NMPC with the current state
    wopt['x',:] = x0

    # Set the bounds for the initial state
    ubw['x', 0] = x0
    lbw['x', 0] = x0

    # Solve the OCP
    solved = trajectory_planner(x0=wopt, p=goal, ubx=ubw, lbx=lbw, ubg=ubg, lbg=lbg)

    if trajectory_planner.stats()['return_status'] == "Solve_Succeeded":
        print("Launch sucessfull =).")
        opt_idx = 0
        wopt = W(solved['x'])
        fail_counter = 0
    else:
        print("Launch failed =(.")
        fail_counter = fail_counter + 1
        opt_idx = fail_counter 
        if fail_counter > max_fail_counter:
            print("Too many fails")
            break

    control = np.array(wopt['u', fail_counter])

    rocket_state = robot_dynamics_function(x0=ca.DM(rocket_state), p=ca.DM(control.flatten()))['xf']

    rocket_state_traj.append(rocket_state)
    control_traj.append(control)

optimal_trajectory_x = [float(x[0]) for x in rocket_state_traj]
optimal_trajectory_vx = [float(x[1]) for x in rocket_state_traj]
optimal_trajectory_y = [float(x[2]) for x in rocket_state_traj]
optimal_trajectory_vy = [float(x[3]) for x in rocket_state_traj]
optimal_trajectory_phi = [float(x[4]) for x in rocket_state_traj]
optimal_trajectory_omega = [float(x[5]) for x in rocket_state_traj]
optimal_trajectory_theta = [float(x[6]) for x in rocket_state_traj]
optimal_control_F = [float(u[0]) for u in control_traj]
optimal_control_theta = [float(u[1]) for u in control_traj]


plt.plot(optimal_trajectory_x, optimal_trajectory_y)
h = 0.15
hr = 0.1
for i in range(len(optimal_trajectory_x)):
    a = [optimal_trajectory_x[i], optimal_trajectory_x[i]+h*np.cos(optimal_trajectory_phi[i])]
    b = [optimal_trajectory_y[i], optimal_trajectory_y[i]+h*np.sin(optimal_trajectory_phi[i])]
    plt.plot(a, b, 'r')
for i in range(len(optimal_trajectory_x)):
    angle = optimal_trajectory_theta[i] + np.pi/180 + optimal_trajectory_phi[i]
    a = [optimal_trajectory_x[i], optimal_trajectory_x[i]-hr*np.cos(angle)]
    b = [optimal_trajectory_y[i], optimal_trajectory_y[i]-hr*np.sin(angle)]
    plt.plot(a, b, 'g')
x = np.linspace(tor_x-5, tor_x+5, 50)
y1 = -(x - tor_x)**2 + tor_lower
y2 = (x - tor_x)**2 + tor_upper
plt.plot(x, y1)
plt.plot(x, y2)
figure = plt.gcf()
figure.set_size_inches(10,5)
plt.grid(True)


fig, axs = plt.subplots(6, 1)
fig.set_size_inches(20,20)
axs[0].plot( optimal_trajectory_vx)
axs[0].grid(True)
axs[0].set_title("vx")
axs[1].plot( optimal_trajectory_vy)
axs[1].grid(True)
axs[1].set_title("vy")
axs[2].plot( optimal_trajectory_phi)
axs[2].grid(True)
axs[2].set_title("phi")
axs[3].plot( optimal_trajectory_theta)
axs[3].grid(True)
axs[3].set_title("theta")
axs[4].plot( optimal_control_F)
axs[4].grid(True)
axs[4].set_title("F")
axs[5].plot( optimal_control_theta)
axs[5].grid(True)
axs[5].set_title("theta")
fig.tight_layout()
plt.show()

